+++
title = "Over mij"
slug = "over"
+++

#### Introductie

Mijn naam is Luciano Nooijen, web en mobile consultant en business analyst. Momenteel ben ik werkzaam bij [Bytecode Digital Agency B.V.](https://bytecode.nl), het bedrijf dat ik samen met Jeroen van Steijn heb opgericht, waar ik de rol als analyst/consultant en technical lead vervul.

#### Werkzaamheden

Het grootste gedeelte van de tijd ben ik bezig met full-stack web/mobile development, waarbij ik het liefst gebruik maak van de allernieuwste technieken.

Wat ik doe draait volledig om business analysis en consultancy. Programmeren op zichzelf is natuurlijk leuk, maar ik geloof erin dat software daadwerkelijk problemen op moet lossen, omdat er anders überhaupt geen reden is om de software te ontwikkelen.

#### Carriëre

Al op jonge leeftijd ben ik begonnen met webdesign en -development. In 2009 maakte ik mijn eerste site, maar vanaf 2014 ben ik het naar een professioneel level gaan tillen. Ik heb eerst een eigen webshop ontwikkeld in WooCommerce en ben daarna websites gaan ontwikkelen onder de naam Nooijen Web Solutions.

Met het team van Freelancers waarmee Nooijen Web Solutions werd gerund is later Bytecode Digital Agency B.V. opgericht, waar ik momenteel werkzaam ben.

#### Vrije tijd

Omdat ik sterk geloof in open source/vrije software probeer ik zo veel mogelijk hieraan bij te dragen met de ontwikkeling waar ik verantwoordelijk voor ben, niet alleen met Bytecode, maar ook daarbuiten.

In mijn vrije tijd speel ik American Football bij de Den Haag Raiders.

#### Website

De broncode van deze website is beschikbaar gesteld op mijn [Github-profiel](https://github.com/lucianonooijen/personal-website) onder MIT-licentie.

Deze website is ontwikkeld in statische website generator Hugo (geschreven in GoLang).

