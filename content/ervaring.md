+++
title = "Professionele werkervaring"
slug = "ervaring"
+++

<style>
h3 em { font-weight: 400; }
h5 { margin-top: 0 !important; }
</style>

### Bytecode Digital Agency */ Co-founder, analyst/consultant, technical lead*

##### 01-05-2018 tot HEDEN

Nooijen Web Solutions bestond uit een team van samenwerkende freelancers. Echter merkten we dat dit met de steeds grotere projecten problemen op kon leveren, mede omdat we geen eigen kantoor hadden. Hierom is Bytecode Digital Agency B.V. opgericht. Hetzelfde team, maar dan een écht vast team (momenteel 4 vaste werknemers), in plaats van een groep freelancers.

Bij Bytecode vervul ik de rol van analyst/consultant en lead-developer en hou ik me ook veel bezig met bijvoorbeeld devops. Vanaf 1 juli 2018 hebben wij een eigen pand in Delfgauw vanuit waar wij onze werkzaamheden verrichten.

Voor meer informatie over Bytecode, zie [bytecode.nl](https://bytecode.nl).

<br>

### Stager Software */ Front-end/full-stack developer*

##### 01-12-2017 tot 01-06-2018

Bij Stager werkte ik als front-end/full-stack developer aan de ticketingservice van Stager. Bij Stager wordt er gewerkt in een scrum-team met sprints van 2 weken. Binnen dit scrum-team werkte ik vooral aan de front-end issues, maar als er zaken in de Java-backend aangepast moesten worden, was dat voor mij ook geen probleem.

<br>

### Nooijen Web Solutions */ Owner, Founder, Full-stack developer*

##### 01-05-2015 tot 01-05-2018

Vanaf 2009 maakte ik regelmatig websites voor vrienden en familie. In 2014 ben ik het professioneler aan gaan pakken. Van af 2015 ben ik gaan werken onder de naam Nooijen Web Solutions. We werkten in een SCRUM-team van 4 freelancers aan verschillende soorten projecten, variërend van one-page marketing-websites tot maatwerkoplossingen.

<br>

### Chemiewinkel.com */ Eigenaar/oprichter*

##### 01-07-2014 tot HEDEN

In de zomervakantie van 3 naar 4 vwo ben ik uit passie voor scheikunde de webwinkel Chemiewinkel.com begonnen. Hiervoor had ik al een paar keer met WordPress gewerkt voor kleinere projecten, maar dit was natuurlijk een geweldige manier om het framework beter te leren kennen. Momenteel staat het op een laag pitje en besteed ik er misschien een uur of twee per week aan.
