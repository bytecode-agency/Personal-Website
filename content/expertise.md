+++
title = "Skills en Expertise"
slug = "expertise"
+++

In de afgelopen tien jaar waarin ik heb geprogrammeerd, heb ik met enorm veel programmeertalen en frameworks te maken gehad, van C tot Ruby en van Java tot Python. Hierdoor heb ik uitgevonden welke talen en frameworks mij het best liggen en waar ik het beste en liefste mee werk.

De afgelopen jaren heb ik me gespecialiseerd in enkele stacks, zoals hieronder te lezen. Hoewel ik natuurlijk mijn voorkeuren heb, zal ik altijd _the best tool for the job_ kiezen, onafhankelijk van mijn voorkeur.

Ik programmeer volgens de Coding Standards van Bytecode, die ik voor het grootste deel heb opgesteld. De Coding Standards zijn [hier](https://github.com/BytecodeBV/Coding-Standards) te vinden.

## Web-development

* Full-stack Javascript (NodeJS, ES6+, Typescript)
* CSS-preprocessors: SASS, Stylus
* ReactJS
* Angular 2+
* WordPress indien een CMS nodig is

## Mobile development

* React Native
* Flutter
* Ionic
* Progressive Web Apps

## Databases

* MySQL, PostgreSQL
* NoSQL, MongoDB
* GraphQL

## Devops

* Amazon Web Services en Digital Ocean
* Docker
* Ansible
* CI/CD

<br>
<hr>
<br>

#### Persoonlijke ontwikkeling

Als developer is het belangrijk om altijd te blijven leren en om ervoor te zorgen dat je altijd bij blijft met de nieuwste technieken. Om deze reden besteed ik één dag per week aan zelf-educatie.

###### Waar ik momenteel mee bezig ben:

* Functioneel programmeren met Elixir (+Phoenix framework)
* Lezen van Clean Code, Robert C. Martin
* Lezen van The C Programming Languate (Kernighan, Ritchie)

###### Waar ik de afgelopen tijd mee bezig ben geweest (meest noemenswaardig):

* Verdiepen in geavanceerde web security en het beveiligen van applicaties
* Bekend raken met het oplossen van ingewikkelde algoritmes
* Flutter voor mobiele ontwikkeling

###### Wat ik nog wil gaan leren (meest noemenswaardig):

* Data science met Python + R
* Artificial Intelligence en Neural Networks met Tensorflow
* Functioneel front-end web development met Elm
* Kubernetes